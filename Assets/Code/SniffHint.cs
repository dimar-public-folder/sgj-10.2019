using UnityEngine;
using UnityEngine.UI;

public class SniffHint : MonoBehaviour {
    [SerializeField] private Text _textField;
    public void SetText (string newText) {
        _textField.text = newText;
    }
}
