using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class DestroyingObject : MonoBehaviour {
    [SerializeField] private float _destroyingVelocity = 1f;
    [SerializeField] private AudioClip _destroySound;
    private List<DestroyedPart> _parts = new List<DestroyedPart>();

    public UnityEvent onBroke;

    // private Rigidbody _rigidbody;

    private void Start() {
        GetComponentsInChildren<DestroyedPart>(true, _parts);
        // _rigidbody = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision other) {
        var noseComp = other.collider.GetComponentInParent<Nose>();
        if (noseComp != null) {
            var noseVelocitySqr = noseComp.velocity.linearVelocity.sqrMagnitude;
            if (noseVelocitySqr >= _destroyingVelocity * _destroyingVelocity) {
                onBroke.Invoke();
                AudioSource.PlayClipAtPoint(_destroySound, transform.position);
            }
            // Debug.Log("nose velocity: " + noseComp.velocity.linearVelocity.ToString());
        }
    }
}
