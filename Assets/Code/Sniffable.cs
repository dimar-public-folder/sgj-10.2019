using System.Collections;
using UnityEngine;

public class Sniffable : MonoBehaviour {
    [SerializeField] private Transform _hintPos;
    [SerializeField] private GameObject _sniffHint;
    // [SerializeField] float _fadeTime = 1f;
    [SerializeField] private string hintText = "занюхан";

    public void OnSniff () {
        _Sniffed();
    }

    private void _Sniffed () {
        var hintPos = transform.position + _hintPos.localPosition * _hintPos.lossyScale.x;
        var hintRot = Camera.main.transform.rotation; // fix rotation here
        var gameObject = Instantiate(_sniffHint, hintPos, hintRot);
        gameObject.GetComponent<SniffHint>().SetText(hintText);
        // _activateOnSniff.gameObject.SetActive(true);
        // yield return new WaitForSeconds(_fadeTime);
        // _activateOnSniff.gameObject.SetActive(false);
    }
}
