using System.Collections;
using UnityEngine;

public class DestroyTimeout : MonoBehaviour {
    [SerializeField] private float _time = 1f;

    private void OnEnable() {
        StartCoroutine(_DestroyCoroutine());
    }

    private IEnumerator _DestroyCoroutine () {
        yield return new WaitForSeconds(_time);
        Destroy(gameObject);
    }
}
