using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sniffer : MonoBehaviour {
    [SerializeField] float _sniffTime = 2f;
    [SerializeField] GameObject _sniffEffect;
    [SerializeField] Transform _sniffOrigin;

    private Dictionary<Sniffable, Coroutine> _sniffings = new Dictionary<Sniffable, Coroutine>();
    private Dictionary<Sniffable, GameObject> _sniffEffects = new Dictionary<Sniffable, GameObject>();

    private void OnTriggerEnter(Collider other) {
        var comp = other.GetComponent<Sniffable>();
        if (comp != null) { // && !comp.isSniffed
            if (!_sniffings.ContainsKey(comp)) {
                _StartSniffing(comp);
            }
        }
    }

    private void OnTriggerExit(Collider other) {
        var comp = other.GetComponent<Sniffable>();
        if (comp != null) { // && !comp.isSniffed
            if (_sniffings.ContainsKey(comp)) {
                _StopSniffing(comp);
            }
        }
    }

    private void _StartSniffing (Sniffable obj) {
        var cor = StartCoroutine(_Sniffing(obj));
        _sniffings.Add(obj, cor);
        Debug.Log("sniffing started " + obj.name);

        var eff = Instantiate(_sniffEffect, obj.transform.position, obj.transform.rotation);
        eff.transform.SetParent(obj.transform);
        eff.GetComponentInChildren<particleAttractorLinear>().target = _sniffOrigin;
        _sniffEffects.Add(obj, eff);
    }

    private void _StopSniffing (Sniffable obj) {
        var cor = _sniffings[obj];
        StopCoroutine(cor);
        _sniffings.Remove(obj);
        Debug.Log("sniffing stopped " + obj.name);
        var eff = _sniffEffects[obj];
        Destroy(eff);
        _sniffEffects.Remove(obj);
    }

    private IEnumerator _Sniffing (Sniffable obj) {
        yield return new WaitForSeconds(_sniffTime);
        obj.OnSniff();
        Debug.Log("sniffing made " + obj.name);
        _StopSniffing(obj);
    }
}
