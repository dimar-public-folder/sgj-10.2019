using UnityEngine;

public class Disassembleable : MonoBehaviour {
    [SerializeField] private GameObject _destroyedPrefab;
    // [SerializeField] private AudioClip _destroySound;
    
    public void Disassembly () {
        // detach parts
        // foreach (var part in _parts)
        // {
        //     part.transform.SetParent(null);
        // }

        // foreach (var part in _parts)
        // {
        //     part.gameObject.SetActive(true);
        // }
        
        var destroyedGO = Instantiate(_destroyedPrefab, transform.position, transform.rotation);
        destroyedGO.transform.localScale = transform.lossyScale;
        // var audioSource = destroyedGO.GetComponentInChildren<AudioSource>();
        // if (audioSource != null && _destroySound != null) {
        //     audioSource.PlayOneShot(_destroySound);
        // }

        // destroy self
        Destroy(gameObject);
    }
}