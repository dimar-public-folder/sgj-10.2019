using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneRestart : MonoBehaviour {
    public float delay = 0f;
    [SerializeField] private List<Light> _toTurnOff; 

    public void Restart () {
        foreach (var light in _toTurnOff) {
            light.enabled = false;
        }

        if (delay > 0f) {
            StartCoroutine(_DelayedRestart());
        } else {
            _DoRestart();
        }
    }

    private IEnumerator _DelayedRestart () {
        yield return new WaitForSeconds(delay);
        _DoRestart();
    }

    private void _DoRestart () {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
