using UnityEngine;

public class VelocityCalc : MonoBehaviour {
    [SerializeField] float _nextStepAlpha = 0.1f;

    public Vector3 linearVelocity {get{return _linVel;}}
    public Vector3 angularVelocity {get{return _angVel;}}

    private Vector3 _linVel = Vector3.zero;
    private Vector3 _angVel = Vector3.zero;
    private Vector3 _prevPos = Vector3.zero;
    private Quaternion _prevRot = Quaternion.identity;

    private void Update() {
        var curLin = (transform.position - _prevPos) / Time.deltaTime;
        var curAng = (Quaternion.Inverse(_prevRot) *  transform.rotation).eulerAngles / Time.deltaTime;

        _prevPos = transform.position;
        _prevRot = transform.rotation;

        var newLin = _linVel * (1-_nextStepAlpha) + curLin * _nextStepAlpha;
        var newAng = _angVel * (1-_nextStepAlpha) + curAng * _nextStepAlpha;

        _linVel = newLin;
        // Debug.Log("linear velocity " + _linVel.ToString());
        _angVel = newAng;
        // Debug.Log("angular velocity " + _angVel.ToString());
    }
}